# -*- coding: utf-8 -*-

import platform
from subprocess import check_output
from flask import Flask, redirect, request, render_template
from utils.decorator_auth import requires_auth

app = Flask(__name__)

@app.before_request
@requires_auth
def basic_auth():
    pass


@app.route("/")
def index():
    return redirect("/info")


@app.route("/info")
def get_info():
    refresh = request.args.get("refresh") or "3"
    ostype = platform.system()
    if ostype == "Linux":
        data = check_output(["top", "-bn", "1"])
    elif ostype == "Darwin":
        data = check_output(["top", "-l", "1"])
    else:
        data = "This Platform is Not Supported"

    return render_template("show.html", refresh=refresh, data=data)


@app.errorhandler(404)
def page_not_found(error):
    return 'Nothing here, sorry', 404

if __name__ == '__main__':
    app.run(debug=True)
